<?php

namespace Redaced\Tokpay\Exceptions;

class ConnectionException extends GatewayException
{
}
