<?php

namespace Redaced\Tokpay\Exceptions;

class UnsupportedLanguageException extends GatewayException
{
}
